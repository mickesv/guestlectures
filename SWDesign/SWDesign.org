#+Title: Software Design
#+Author: Mikael Svahnberg
#+Email: Mikael.Svahnberg@bth.se
#+Date: 2017-01-24
#+EPRESENT_FRAME_LEVEL: 1
#+OPTIONS: email:t <:t todo:t f:t ':t H:1
# #+STARTUP: showall
#+STARTUP: beamer

# #+LATEX_HEADER: \usepackage[a4paper]{geometry}
#+LATEX_CLASS_OPTIONS: [10pt,t,a4paper]
# #+LATEX_CLASS: beamer
#+BEAMER_THEME: BTH_msv

* Levels of Design
  - Functions / Lines of Code
  - Classes, Groups of Classes
  - Modules, Subsystems
  - Software Architecture
  - Systems Architecture
* Fundamental Design Principles
#+NAME: tab:FundamentalDesignPrinciples
#+CAPTION: Fundamental Design Principles
#+ATTR_LATEX: :font \scriptsize :align p{5cm}p{5cm}
   | Principle                                     | Description                                                                                                                                    |
   |-----------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------|
   | Abstraction                                   | Focus on relevant properties                                                                                                                   |
   | Coupling and Cohesion                         | Independent entities, but where the internals are strongly related                                                                             |
   | Decomposition and Modularisation              | Small entities with well defined and non-overlapping functionalities or responsibilities                                                       |
   | Encapsulation                                 | Hide the internals so they are inaccessible from outside the entity                                                                            |
   | Separation of Interface and Implementation    | Separate the public interface from implementation details                                                                                      |
   | Sufficiently, Completeness, and Primitiveness | Keep entities as small as possible, but not smaller. Entities should capture all important characteristica of an abstraction but nothing more. |
   |-----------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------|
* Responsibility-Driven Design
  - Low-level Design Patterns
    - Information Expert
    - Creator
    - Controller
    - Low Coupling
    - High Cohesion
    - Polymorphism
    - Pure Fabrication
    - Indirection
    - Protected Variations
* Classes (Design Patterns)
  - Interactions between groups of classes to solve a particular problem

#+NAME: tab:GoFPatterns
#+ATTR_LATEX: :font \scriptsize :align p{1.5cm}p{9cm}
   | Pattern          | Brief Problem Description                                                                                                                                                                        |
   |------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
   | Adapter          | You want to use one interface, but your code provides another, or another version.                                                                                                               |
   | Factory          | You need a transparent way to create objects of the right type.                                                                                                                                  |
   | Singleton        | You have a resource that is shared across your entire application and which must be kept unique, but can not/do not want/your software design teacher has told you not to/ use a global variable |
   |                  | (Note, a global variable would not solve the "unique access" problem anyway)                                                                                                                     |
   | Strategy         | You need a way to (possibly at runtime) change an algorithm depending on some circumstances.                                                                                                     |
   | Composite        | In your system, the same strategies should be applied to individual entities as to some, defined, groupings of entities.                                                                         |
   | Facade           | You have different implementations or interfaces that you wish to hide from the users of your API.                                                                                               |
   | Observer         | You want to be able to add new parts to your system that should be notified of changes in another part, without having to go in there and hard-code the notification all the time.               |
   | Proxy            | You do not want to directly access a resource, for a number of reasons.                                                                                                                          |
   | Abstract Factory | Your homegrown Factory pattern just blew up in your face and you need a more scalable solution.                                                                                                  |
   |------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
* Discussion: Quality Attributes				 :Discussion:
  Design Patterns address a few specific challenges:

  - Which?
  - How?
  - Why focus on these?
* Software (and Systems) Architecture
  - Higher level:
    - Concepts and Components
    - Modules and Submodules
    - Processes and Threads -- Hardware Units
    - Packages and Files
* Architecture and Quality
  Imagine a home:
  - Comfortable
  - Cosy
  - Nice Reading Corner
  - "Kitchen Lap"
  
  Compare with a Museum
  - Bright
  - Easy access
  - Resting areas
  - Emergency exits
  - Easy to clean
* Software Architecture and Quality				 :Discussion:
  
  - Examples of qualities that you can achieve in software
  - How?

* Cave
  The architecture provides a /potential/ for quality attributes. This will help your (excellent) software developers.

  However:
  - Brillant software developers can reach your quality goals anyway.
  - Conversely, less skilled software developers can hinder your ability to reach your quality goals.
* Examples of factors where the architecture may influence the results
  :PROPERTIES:
  :BEAMER_OPT: shrink=15
  :END:

#+BEAMER: \pause
  - Product
    - Functional Features
    - User Interface
    - Performance
    - Dependability
    - Failure Detection, Reporting, Recovery
    - Service
    - Product Cost
#+BEAMER: \pause
  - Technology
    - General-Purpose Hardware
    - Domain-Specific Hardware
    - Software Technology
    - Architecture Technology
    - Standards
#+BEAMER: \pause
  - Organisation
    - Management
    - Staff Skills, Interests, Strengths, Weaknesses
    - Process and Development Environment
    - Development Schedule
    - Development Budget
* Discusssion: Factors						 :Discussion:
  - How do these factors influence the design?
  - Why is it important to know this early?
* Discussion: Organisational Factors				 :Discussion:
  - In particular, how and why do the /organisational factors/ influence the design?
** Organisational Factors
#+BEAMER: \only<1>{
      - Management
      - Staff Skills, Interests, Strengths, Weaknesses
      - Process and Development Environment
      - Development Schedule
      - Development Budget
#+BEAMER: }\only<2>{
    - Management
      - Build versus Buy
      - Schedule versus Functionality
      - Environment
      - Business Goals
    - Staff skills, interests, strengths, weaknesses
#      - Application Domain
#      - Software Design
#      - Specialised imlementation techniques
#      - Specialised analysis techniques
    - Process and development environment
#      - Development platform
#      - Development process and tools
#      - Configuration management process and tools
#      - Production process and tools
#      - Testing process and tools
#      - Release process and tools
    - Development schedule
#      - Time-to-market
#      - Delivery of features
#      - Release schedule
    - Development budget
#      - Head count
#      - Cost of development tools
#+BEAMER: }
* Engineering Concerns and Views				 :Discussion:
  - An architecture is developed from different viewpoints
  - Each viewpoint focus on different engineering concerns
** Engineering Concerns
#+BEAMER: \only<1>{
- Conceptual View
- Module View
- Execution View
- Code View
#+BEAMER: }\only<2>{
- Conceptual View
  - How does the system fulfill the requirements?
  - How are COTS components to be integrated? How do they interact with the rest of the system?	
  - How is domain specific hardware and/or software incorporated into the system?	
  - How is functionality partitioned into product releases?	
  - How does the system incorporate portions of the prior generations of the product and how will it support future generations?	
  - How are product lines supported?	
  - How can the impact of changes in requirements or the domain be minimized?	
- Module View
- Execution View
- Code View    
#+BEAMER: }\only<3>{
- Conceptual View
- Module View
  - How is the product mapped to the software platform?	
  - What system support/services does it use, and exactly where?	
  - How can testing be supported?	
  - How can dependencies between modules be minimised?	
  - How can reuse of modules and subsystems be maximised?	
  - What techniques can be used to insulate the product from changes in COTS software, in the software platform, or changes to standards?
- Execution View
- Code View   
#+BEAMER: }\only<4>{
- Module View
- Execution View
  - How does the system meet its performance, recovery and reconfiguration requirements?	
  - How can one balance resource usage (for example, load balancing)?	
  - How can one achieve the necessary concurrency, replication and distribution without adding too much complexity to the control algorithms?	
  - How can the impact of changes in the runtime platform be minimised?	
- Code View
#+BEAMER: }\only<5>{
- Conceptual View
- Module View
- Execution View
- Code View
  - How can the time and effort for product upgrades be reduced?	
  - How should product versions and releases be managed?	
  - How can build time be reduced?	
  - What tools are needed to support the development environment?	
  - How are integration and testing supported?	
#+BEAMER: }
* Architecture Patterns
  - Starting points for architecture design.
  - /Not/ like Design Patterns.
    - A Design pattern addresses one particular type of problem
    - An architecture pattern is an overall structure with certain characteristica (quality attributes)
  - Common architecture patterns:
    - Layered
    - Model-View-Controller
    - Client-Server
    - (Cloud) Queue-Centric Workflow
* One more thing: Architecture Evaluations
  - So much depend on having the right architecture
  - therefore, /architecture evaluations/ are a common early milestone in a project
  - Connect back to the (for your project) important factors: Will you hit them?
** Early Evaluation Questions
  - Do we meet the quality requirements on the system?
  - Do all stakeholders share a common understanding of the system?
  - Are all requirements accounted for?
  - Are there any weak spots in ther architecture?
  - Can the system (and/or the architecture) be improved?
  - Does the development team have all the necessary resources?
  - Should we let this project continue?
* Summary
  - Design is more than just maintainability
  - The right tool (design / architecture) for the right problem
  - Design is important and should be allowed to take time
